from fastapi import APIRouter, HTTPException
from app.api.models import Mahasiswa
from app.api.db_manager import add_mahasiswa, get_mahasiswa, update_mahasiswa

update_service = APIRouter()
@update_service.post('/', status_code=201)
async def update_mahasiswa_service(payload: Mahasiswa):
    mahasiswa = await get_mahasiswa(payload.npm)
    if (mahasiswa != None):
        await update_mahasiswa(payload.npm, payload.nama)
        return {
            'status': 'OK'
        }
    await add_mahasiswa(payload)
    return {
        'status': 'OK'
    }