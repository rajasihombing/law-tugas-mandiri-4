from app.api.db import mahasiswa, database

# async def get_all_mahasiswa():
#     query = mahasiswa.select()
#     return await database.fetch_all(query=query)

async def add_mahasiswa(payload: mahasiswa):
    query = mahasiswa.insert().values(**payload.dict())
    return await database.execute(query=query)

async def get_mahasiswa(npm):
    query = mahasiswa.select(mahasiswa.c.npm==npm)
    return await database.fetch_one(query=query)

async def update_mahasiswa(npm, nama):
    query = mahasiswa.update().where(mahasiswa.c.npm == npm).values(nama= nama)
    return await database.execute(query=query)
